export interface UserInfo{
    username?: string;
    realname?: string;
    avatar?: string;
    email?: string;
    phone?: string;
}

export interface UserToken{
    userToken?:string;
}
