const modules = {
    'Layout': () => import('@/views/layout/Index.vue'),
    '@/views/home/Index.vue': () => import('@/views/home/Index.vue'),
    '@/views/system/sysDept/SysDeptList.vue': () => import('@/views/system/sysDept/SysDeptList.vue'),
    '@/views/system/sysResource/SysResourceList.vue': () => import('@/views/system/sysResource/SysResourceList.vue'),
    '@/views/system/sysRole/SysRoleList.vue': () => import('@/views/system/sysRole/SysRoleList.vue'),
    '@/views/system/sysUser/SysUserList.vue': () => import('@/views/system/sysUser/SysUserList.vue')
}
export default modules
