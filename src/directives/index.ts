import type { App } from "vue";


// 自定义权限控制指令
import HasPermissionDirective from './hasPermission';

// 全局注册 directive
export function setupDirective(app: App<Element>) {
    // 引入指令
    app.directive('hasPermission', HasPermissionDirective);
}
