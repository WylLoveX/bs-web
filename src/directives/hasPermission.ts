import { DirectiveBinding } from "vue";
import userStore from "@/store/modules/user";
/**
 * 按钮权限
 */
export default {
    // 自定义指令名称
    name: 'hasPermission',
    // 在绑定元素挂载时执行
    mounted(el: HTMLElement, binding: DirectiveBinding) {
        const useUserStore = userStore()
        const { value } = binding; // value应为当前需要验证的权限标识
        if(value){
            // 「超级管理员」拥有所有的按钮权限
            const btnList = useUserStore.getUserBtnList;

            // 检查用户是否有指定的权限
            const hasPerm = btnList.includes(value);

            // 根据权限结果进行操作
            if (!hasPerm) {
                el.parentNode && el.parentNode.removeChild(el);
            }
        }else{
            throw new Error(
                "资源标识不正确! 参考 v-has:permission=\"['sys:user:add']\""
            );
        }

    },

    // 当v-has_permission的值发生变化时执行
    updated(el: HTMLElement, binding: DirectiveBinding) {
        // 可以在这里重新检查权限变化，并更新按钮状态
        this.mounted(el, binding);
    },
};
