/**
 * 登录请求参数
 */
export interface QueryParams extends PageParams{

}

/**
 * 实体信息
 */
export interface SysDept{
    id?:string,
    parentId?:string,
    isLeaf?:number,
    deptName?:string,
    deptOrder?:number,
    deptDesc?:string,
    deptCategory?:number,
    address?:string,
    delFlag?:number,
    createBy?:string,
    createTime?:Date,
    updateBy?:string,
    updateTime?:Date,
}
/**
 * 表单信息
 */
export interface SysDeptForm{
    id?:string,
    parentId?:string,
    isLeaf?:number,
    deptName?:string,
    deptOrder?:number,
    deptDesc?:string,
    deptCategory?:number,
    address?:string,
    delFlag?:number,
}
