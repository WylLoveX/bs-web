import request from '@/utils/request'
import {QueryParams,SysDept} from "@/api/system/sysDept/sysDeptType";

export function list(queryParams?: QueryParams) {
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysDept/list',
            params:queryParams,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function queryById(id?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysDept/queryById?id='+id,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function updateById(sysDept?:SysDept){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysDept/updateById',
            data:sysDept,
            method: 'POST'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function removeByIds(ids?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysDept/removeByIds?ids='+ids,
            method: 'delete'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function removeById(id?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysDept/removeById?id='+id,
            method: 'delete'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function save(sysDept?:SysDept){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysDept/save',
            data:sysDept,
            method: 'post'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}
