/**
 * 登录请求参数
 */
export interface QueryParams extends PageParams{

}

/**
 * 实体信息
 */
export interface SysUserRole{
    id?:string,
    userId?:string,
    roleId?:string,
    delFlag?:number,
    createBy?:string,
    createTime?:Date,
    updateBy?:string,
    updateTime?:Date,
}
/**
 * 表单信息
 */
export interface SysUserRoleForm{
    id?:string,
    userId?:string,
    roleId?:string,
    delFlag?:number,
}
