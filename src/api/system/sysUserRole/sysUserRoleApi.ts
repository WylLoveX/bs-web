import request from '@/utils/request'
import {QueryParams,SysUserRole} from "@/api/system/sysUserRole/sysUserRoleType";

export function list(queryParams?: QueryParams) {
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUserRole/list',
            params:queryParams,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function queryById(id?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUserRole/queryById?id='+id,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function updateById(sysUserRole?:SysUserRole){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUserRole/updateById',
            data:sysUserRole,
            method: 'POST'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function removeByIds(ids?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUserRole/removeByIds?ids='+ids,
            method: 'delete'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function removeById(id?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUserRole/removeById?id='+id,
            method: 'delete'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function save(sysUserRole?:SysUserRole){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUserRole/save',
            data:sysUserRole,
            method: 'post'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}
