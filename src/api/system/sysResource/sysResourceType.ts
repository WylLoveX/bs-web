/**
 * 登录请求参数
 */
export interface QueryParams{
    parentId?:string
}

/**
 * 实体信息
 */
export interface SysResource{
    id?:string,
    parentId?:string,
    resourceName?:string,
    menuUrl?:string,
    menuComponent?:string,
    redirect?:string,
    resourceType?:number,
    perms?:string,
    sortNo?:number,
    menuIcon?:string,
    isRoute?:number,
    isLeaf?:number,
    description?:string,
    createBy?:string,
    createTime?:Date,
    updateBy?:string,
    updateTime?:Date,
    delFlag?:number,
}
/**
 * 表单信息
 */
export interface SysResourceForm{
    id?:string,
    parentId?:string,
    resourceName?:string,
    menuUrl?:string,
    menuComponent?:string,
    redirect?:string,
    resourceType?:number,
    perms?:string,
    sortNo?:number,
    menuIcon?:string,
    isRoute?:number,
    isLeaf?:number,
    description?:string,
    delFlag?:number,
}
