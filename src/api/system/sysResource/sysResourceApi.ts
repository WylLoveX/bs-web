import request from '@/utils/request'
import {QueryParams,SysResource} from "@/api/system/sysResource/sysResourceType";

export function list(queryParams?: QueryParams) {
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysResource/list',
            params:queryParams,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function queryById(id?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysResource/queryById?id='+id,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function updateById(sysResource?:SysResource){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysResource/updateById',
            data:sysResource,
            method: 'POST'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function removeByIds(ids?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysResource/removeByIds?ids='+ids,
            method: 'delete'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function removeById(id?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysResource/removeById?id='+id,
            method: 'delete'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function save(sysResource?:SysResource){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysResource/save',
            data:sysResource,
            method: 'post'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function getSysResourceTableTree(queryParams?: QueryParams) {
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysResource/getSysResourceTableTree',
            params:queryParams,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}
export function getMenuTree(){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysResource/getMenuTree',
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    })
}

