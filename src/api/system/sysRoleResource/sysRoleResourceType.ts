/**
 * 角色资源权限
 */
import {TreeKey} from "element-plus/lib/components/tree/src/tree.type";

export interface RoleResources{
    roleId?:string,
    checkedKeys?:TreeKey[],
    halfCheckedKeys?:TreeKey[]
}
