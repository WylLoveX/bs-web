import request from '@/utils/request'
import {RoleResources} from "@/api/system/sysRoleResource/sysRoleResourceType";


export function updateRoleResources(roleResources?:RoleResources){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysRoleResource/updateRoleResources',
            data:roleResources,
            method: 'post'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    })
}
