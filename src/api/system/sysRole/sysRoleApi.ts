import request from '@/utils/request'
import {QueryParams,SysRole} from "@/api/system/sysRole/sysRoleType";

export function list(queryParams?: QueryParams) {
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysRole/list',
            params:queryParams,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function queryById(id?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysRole/queryById?id='+id,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function updateById(sysRole?:SysRole){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysRole/updateById',
            data:sysRole,
            method: 'POST'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function removeByIds(ids?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysRole/removeByIds?ids='+ids,
            method: 'delete'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function removeById(id?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysRole/removeById?id='+id,
            method: 'delete'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function save(sysRole?:SysRole){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysRole/save',
            data:sysRole,
            method: 'post'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function querySysRoleSelectList(){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysRole/querySysRoleSelectList',
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function getSysResourceSelectTree(roleId?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysRole/getSysResourceSelectTree?roleId='+roleId,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    })
}
