/**
 * 登录请求参数
 */
export interface QueryParams extends PageParams{

}

/**
 * 实体信息
 */
export interface SysRole{
    id?:string,
    roleName?:string,
    roleCode?:string,
    roleDesc?:string,
    delFlag?:number,
    createBy?:string,
    createTime?:Date,
    updateBy?:string,
    updateTime?:Date,
}
/**
 * 表单信息
 */
export interface SysRoleForm{
    id?:string,
    roleName?:string,
    roleCode?:string,
    roleDesc?:string,
    delFlag?:number,
}
