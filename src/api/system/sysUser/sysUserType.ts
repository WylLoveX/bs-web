/**
 * 登录请求参数
 */
export interface QueryParams extends PageParams{

}

/**
 * 实体信息
 */
export interface SysUser{
    id?:string,
    username?:string,
    realname?:string,
    password?:string,
    avatar?:string,
    email?:string,
    phone?:string,
    deptId?:string,
    status?:number,
    delFlag?:number,
    duties?:string,
    roleIdList?:[],
    createBy?:string,
    createTime?:Date,
    updateBy?:string,
    updateTime?:Date,
}
/**
 * 表单信息
 */
export interface SysUserForm{
    id?:string,
    username?:string,
    realname?:string,
    password?:string,
    avatar?:string,
    email?:string,
    phone?:string,
    deptId?:string,
    status?:number,
    delFlag?:number,
    duties?:string,
    roleIdList?:[],
}
