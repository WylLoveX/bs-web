import request from '@/utils/request'
import {QueryParams,SysUser} from "@/api/system/sysUser/sysUserType";

export function list(queryParams?: QueryParams) {
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUser/list',
            params:queryParams,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function queryById(id?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUser/queryById?id='+id,
            method: 'get'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function updateById(sysUser?:SysUser){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUser/updateById',
            data:sysUser,
            method: 'POST'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function removeByIds(ids?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUser/removeByIds?ids='+ids,
            method: 'delete'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function removeById(id?:any){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUser/removeById?id='+id,
            method: 'delete'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}

export function save(sysUser?:SysUser){
    return new Promise<void>((resolve, reject) => {
        return request({
            url:'/system/sysUser/save',
            data:sysUser,
            method: 'post'
        }).then((res?:any)=>{
            resolve(res)
        }).catch((error)=>{
            reject(error)
        })
    });
}
