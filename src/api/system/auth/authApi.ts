import request from '@/utils/request'
import {LoginParams} from "@/api/system/auth/authType";

// 登陆
export function userLoginApi(loginParams: LoginParams) {
  return new Promise<void>((resolve, reject) => {
    return request({
      url:'/system/login/loginByUsernameAndPassword',
      data:loginParams,
      method: 'post'
    }).then((res:any)=>{
      resolve(res)
    }).catch((error)=>{
      reject(error)
    })
  });
}

// 退出登录
export function userLogoutApi() {
  return new Promise<void>((resolve, reject) => {
    return request({
      url:'/system/login/logout',
      method: 'get'
    }).then((res:any)=>{
      resolve(res)
    }).catch((error)=>{
      reject(error)
    })
  });
}

// 获取用户信息
export function getUserInfoByTokenApi() {
  return new Promise<void>((resolve, reject) => {
    return request({
      url:'/system/login/getUserInfoByToken',
      method: 'get'
    }).then((res:any)=>{
      resolve(res)
    }).catch((error)=>{
      reject(error)
    })
  });
}

export function getUserResourceByTokenApi(){
  return new Promise<void>((resolve, reject) => {
    return request({
      url:'/system/login/getUserResourceByToken',
      method: 'get'
    }).then((res:any)=>{
      resolve(res)
    }).catch((error)=>{
      reject(error)
    })
  });
}
