/**
 * 登录请求参数
 */
export interface LoginParams {
    /**
     * 用户名
     */
    username?: string;
    /**
     * 密码
     */
    password?: string;
}

/**
 * 菜单项
 */
export interface MenuItem{
    /**
     * 名称
     */
    name?: string;
    /**
     * 访问路径
     */
    path?: string;
    /**
     * 组件路径
     */
    component?: string;
    /**
     * 跳转路径
     */
    redirect?: string;
    /**
     * 基础信息，包括标题，菜单图标
     */
    meta?: {
        title?: string;
        icon?: string;
    };
    /**
     * 子菜单
     */
    children?: MenuItem[];
}

