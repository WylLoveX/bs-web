declare global {
    /**
     * 分页参数
     */
    interface PageParams {
        // 当前页
        currentPage: number;
        // 每页数据条数
        pageSize: number;
    }
}
export {}

