import { createApp } from 'vue'
import App from './App.vue'

//引入样式配置
import 'normalize.css'
import './styles/index.scss'
//引入element-plus
import ElementPlus from 'element-plus'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
zhCn.el.pagination.total = '共' + `{total}` + '项数据'
zhCn.el.pagination.goto = '跳至'
import 'element-plus/dist/index.css'
//引入路由
import router from './router'
//引入store
import store from './store'
// svg注册脚本
import 'virtual:svg-icons-register'
import {setupDirective} from "@/directives";

const app = createApp(App)
//引入element-plus
app.use(ElementPlus,{ locale: zhCn })
//引入路由
app.use(router)
//引入store
app.use(store)
// 注册自定义指令
setupDirective(app)
app.mount('#app')
