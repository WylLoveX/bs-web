/**
 * @description 文档注册enter事件
 * @param {any} cb
 * @return {void}
 */
export const handleEnter = (cb: Function): void => {
    document.onkeydown = e => {
        const ev: KeyboardEventInit = window.event || e;
        if (ev.keyCode === 13) {
            cb();
        }
    };
};

// 其中 KeyboardEventInit 为内置，以下是代码截取
interface KeyboardEventInit extends EventModifierInit {
    /** @deprecated */
    charCode?: number;
    code?: string;
    isComposing?: boolean;
    key?: string;
    /** @deprecated */
    keyCode?: number;
    location?: number;
    repeat?: boolean;
}
