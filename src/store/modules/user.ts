import { defineStore } from 'pinia'
const userStore = defineStore('user', {
    state: () => ({
        userInfo:{
            userId:'',
            username: '',
            realName: '',
            avatar: '',
            email: '',
            phone: '',
            deptId: '',
            deptName: '',
            roles: [] as any
        },
        userToken: '',
        perms:{
            menuList: [] as any,
            btnList: [] as any,
            routerList: [] as any
        },
    }),
    persist: true,
    getters:{
        getUserInfo:(state)=>state.userInfo,
        getUserToken:(state)=>state.userToken,
        getUserMenuList:(state)=>state.perms.menuList,
        getUserRouterList:(state)=>state.perms.routerList,
        getUserBtnList:(state)=>state.perms.btnList
    },
    actions: {
        SET_USER_INFO(info: any) {
            this.userInfo = info
        },
        SET_USER_TOKEN(token: any){
            this.userToken = token
        },
        SET_MENU_LIST(menuList: any){
            this.perms.menuList = menuList
        },
        SET_ROUTER_LIST(routerList: any){
            this.perms.routerList = routerList
        },
        SET_BTN_LIST(btnList: any){
            this.perms.btnList = btnList
        },
        LOGOUT() {
            this.userInfo = {
                userId:'',
                username: '',
                realName: '',
                avatar: '',
                email: '',
                phone: '',
                deptId: '',
                deptName: '',
                roles: []
            }
            this.userToken = ''
            this.perms = {
                menuList: [],
                btnList: []
            }
        }
    }
})
export default userStore
