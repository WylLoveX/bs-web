import { createPinia } from 'pinia'
import {createPersistedState} from 'pinia-plugin-persistedstate'
const store = createPinia()
store.use(
    createPersistedState({
        serializer: {
            deserialize: JSON.parse,
            serialize: JSON.stringify
        },
    })
)
export default store
