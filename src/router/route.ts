import type { RouteRecordRaw } from 'vue-router'
import {createRouter, createWebHistory} from "vue-router";

// 公共路由
const constantRoutes: RouteRecordRaw[] = [
    {
        path: '/',
        redirect: '/login',
    },
    {
        path: '/login',
        component: () => import('@/views/login/index.vue'),
        name: 'Login',
        meta: { title: '登录' },
    },
    {
        path: '/404',
        component: () => import('@/views/error-page/404.vue'),
        name: '404',
        meta: { title: '404' },
    },
]
export const dynamicRouter:RouteRecordRaw[] = []

const router = createRouter({
    history: createWebHistory('/'),
    routes: constantRoutes
})
export default router;
